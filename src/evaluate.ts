import { Environment } from './environment';

export function evaluate(exp: INode, env: Environment): any {
  switch (exp.type) {
    case 'num':
    case 'str':
    case 'bool':
      return (<IPrimitive>exp).value;
    case 'var':
      return env.get((<IVariableNode>exp).value);
    case 'assign':
      if ((<IAssignNode>exp).left.type != 'var') {
        throw new Error(`Cannot assign to ${JSON.stringify(exp.left)}`);
      }
      return env.set((<IAssignNode>exp).left.value, evaluate(exp.right!, env));
    case 'binary':
      return apply_op(
        (<IBinaryNode>exp).operator,
        evaluate(exp.left!, env),
        evaluate(exp.right!, env)
      );
    case 'lambda':
      return make_lambda(env, <ILambdaNode>exp);
    case 'if':
      const condition = evaluate((<IIfNode>exp).cond, env);
      if (condition !== false) {
        return evaluate(exp.then!, env);
      }
      return exp.else ? evaluate(exp.else, env) : false;
    case 'prog':
      let val: any = false;
      (<IProgramNode>exp).prog.forEach(subExp => {
        val = evaluate(subExp, env);
      });
      return val;
    case 'call':
      const func = evaluate((<ICallNode>exp).func, env);
      return func.apply(null, (<ICallNode>exp).args.map(a => evaluate(a, env)));
    default:
      throw new Error(`I don't know how to evaluate ${exp.type}`);
  }
}

function apply_op(op: string, a: any, b: any): any | never {
  const num = (x: number): number | never => {
    if (typeof x != 'number') throw new Error(`Expected number but got ${x}`);

    return x;
  };
  const div = (x: number): number | never => {
    if (num(x) == 0) throw new Error('Divide by zero');

    return x;
  };

  switch (op) {
    case '+':
      return num(a) + num(b);
    case '-':
      return num(a) - num(b);
    case '*':
      return num(a) * num(b);
    case '/':
      return num(a) / div(b);
    case '%':
      return num(a) % div(b);
    case '&&':
      return a !== false && b;
    case '||':
      return a !== false ? a : b;
    case '<':
      return num(a) < num(b);
    case '>':
      return num(a) > num(b);
    case '<=':
      return num(a) <= num(b);
    case '>=':
      return num(a) >= num(b);
    case '==':
      return a === b;
    case '!=':
      return a !== b;
  }
  throw new Error(`Can't apply operator ${op}`);
}

function make_lambda(env: Environment, exp: ILambdaNode) {
  function lambda() {
    const names = exp.vars;
    const scope = env.extend();
    names.forEach((name, i, me) => {
      scope.def(name, i < arguments.length ? arguments[i] : false);
    });
    return evaluate(exp.body, scope);
  }
  return lambda;
}
