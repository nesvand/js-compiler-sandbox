interface INode {
  type: string;
  value?: number | string | boolean;
  cond?: INode;
  then?: INode;
  else?: INode;
  vars?: Array<string | IVariableNode>;
  body?: INode;
  func?: IVariableNode;
  args?: Array<INode>;
  operator?: string;
  left?: INode;
  right?: INode;
  prog?: Array<INode>;
}

// Primitives
interface IPrimitive extends INode {
  value: number | string | boolean;
}

interface INumberNode extends IPrimitive {
  type: 'num';
  value: number;
}

interface IStringNode extends IPrimitive {
  type: 'str';
  value: string;
}

interface IBooleanNode extends IPrimitive {
  type: 'bool';
  value: boolean;
}

interface IVariableNode extends INode {
  type: 'var';
  value: string;
}

// Keywords
interface IIfNode extends INode {
  type: 'if';
  cond: INode;
  then: INode;
}

interface ILambdaNode extends INode {
  type: 'lambda';
  vars: Array<string>;
  body: INode;
}

// Operations
interface ICallNode extends INode {
  type: 'call';
  func: IVariableNode;
  args: Array<INode>;
}

interface IBinaryNode extends INode {
  type: 'binary';
  operator: string;
  left: INode;
  right: INode;
}

interface IAssignNode extends INode {
  type: 'assign';
  operator: string;
  left: IVariableNode;
  right: INode;
}

// Program
interface IProgramNode extends INode {
  type: 'prog';
  prog: Array<INode>;
}

// Scope
interface ILetNode extends INode {
  type: 'let';
  vars: Array<IVariableNode>;
  body: INode;
}
