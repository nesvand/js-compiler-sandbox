interface IToken extends INode {}

interface IOperatorToken extends IToken {
  type: 'op';
  value: string;
}

interface IPunctuationToken extends IToken {
  type: 'punc';
  value: string;
}

interface IKeywordToken extends IToken {
  type: 'kw';
  value: string;
}
