interface IVars extends Object {
  [keys: string]: any;
}

export class Environment {
  private vars: IVars;
  constructor(private parent: Environment | null = null) {
    this.vars = Object.create(this.parent ? this.parent.vars : null);
  }

  extend() {
    return new Environment(this);
  }

  lookup(name: string): Environment | null {
    let scope: Environment | null = this;
    while (scope) {
      if (Object.prototype.hasOwnProperty.call(scope.vars, name)) {
        return scope;
      }
      scope = scope.parent;
    }

    return null;
  }

  get(name: string) {
    if (name in this.vars) {
      return this.vars[name];
    }

    throw new Error(`Undefined variable "${name}"`);
  }

  set<T>(name: string, value: T): T {
    const scope = this.lookup(name);

    if (!scope && this.parent) {
      throw new Error(`Undefined variable "${name}"`);
    }

    return ((scope || this).vars[name] = value);
  }

  def<T>(name: string, value: T): T {
    return (this.vars[name] = value);
  }
}
