import { InputStream } from './input-stream';

type TPredicate = (ch: string) => boolean;

export class TokenStream {
  private inputStream: InputStream;
  private keywords = ' if then else lambda λ true false ';
  private current: IToken | null = null;
  public croak: (msg: string) => never;

  constructor(input: string) {
    this.inputStream = new InputStream(input);
    this.croak = this.inputStream.croak;
  }

  private isKeyword(x: string): boolean {
    return this.keywords.indexOf(` ${x} `) >= 0;
  }

  private isDigit(ch: string): boolean {
    return /[0-9]/i.test(ch);
  }

  private isIdStart(ch: string): boolean {
    return /[a-zλ_]/i.test(ch);
  }

  private isId(ch: string): boolean {
    return this.isIdStart(ch) || '?!-<>=0123456789'.indexOf(ch) >= 0;
  }

  private isOpChar(ch: string): boolean {
    return '+-*/%=&|<>!'.indexOf(ch) >= 0;
  }

  private isPunctuation(ch: string): boolean {
    return ',;(){}[]'.indexOf(ch) >= 0;
  }

  private isWhitespace(ch: string): boolean {
    return ' \t\n'.indexOf(ch) >= 0;
  }

  private readWhile(predicate: TPredicate) {
    let str = '';

    while (
      !this.inputStream.eof() &&
      predicate.call(this, [this.inputStream.peek()])
    )
      str += this.inputStream.next();

    return str;
  }

  private readNumber(): IToken {
    let has_dot = false;

    const number = this.readWhile((ch: string) => {
      if (ch == '.') {
        if (has_dot) return false;
        has_dot = true;
        return true;
      }
      return this.isDigit(ch);
    });

    return { type: 'num', value: parseFloat(number) };
  }

  private readIdentifier(): IToken {
    const id = this.readWhile(this.isId);

    return {
      type: this.isKeyword(id) ? 'kw' : 'var',
      value: id
    };
  }

  private readEscapedString(end: string): string {
    let escaped = false;
    let str = '';

    this.inputStream.next();

    while (!this.inputStream.eof()) {
      const ch = this.inputStream.next();

      if (escaped) {
        str += ch;
        escaped = false;
      } else if (ch === '\\') {
        escaped = true;
      } else if (ch === end) {
        break;
      } else {
        str += ch;
      }
    }

    return str;
  }

  private readString(): IToken {
    return { type: 'str', value: this.readEscapedString('"') };
  }

  private skipComment(): void {
    this.readWhile((ch: string) => {
      return ch !== '\n';
    });

    this.inputStream.next();
  }

  private readNext(): IToken | null {
    this.readWhile(this.isWhitespace);

    if (this.inputStream.eof()) return null;

    const ch = this.inputStream.peek();

    if (ch === '#') {
      this.skipComment();
      return this.readNext();
    }

    if (ch === '"') return this.readString();

    if (this.isDigit(ch)) return this.readNumber();

    if (this.isIdStart(ch)) return this.readIdentifier();

    if (this.isPunctuation(ch))
      return {
        type: 'punc',
        value: this.inputStream.next()
      };

    if (this.isOpChar(ch))
      return {
        type: 'op',
        value: this.readWhile(this.isOpChar)
      };

    return this.inputStream.croak("Can't handle character: " + ch);
  }

  peek(): IToken | null {
    return this.current || (this.current = this.readNext());
  }

  next(): IToken | null {
    const tok = this.current;

    this.current = null;

    return tok || this.readNext();
  }

  eof(): boolean {
    return this.peek() === null;
  }
}
