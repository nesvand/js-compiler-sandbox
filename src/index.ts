import { Parser } from './parser';
import fs from 'fs';
import util from 'util';
import { Environment } from './environment';
import { evaluate } from './evaluate';

const readFile = util.promisify(fs.readFile);

async function readInput() {
  try {
    const input = await readFile('INPUT', {
      encoding: 'utf8'
    });

    const parser = new Parser(input);
    const ast = parser.parseInput();

    console.log(JSON.stringify(ast, null, 2));

    const globalEnv = new Environment();
    globalEnv.def('print', (str: string) => {
      console.log(str);
    });

    evaluate(ast, globalEnv);
  } catch (e) {
    console.error(e);
  }
}

readInput();
