import { TokenStream } from './token-stream';

const FALSE: IBooleanNode = { type: 'bool', value: false };
const PRECEDENCE: { [key: string]: number } = {
  '=': 1,
  '||': 2,
  '&&': 3,
  '<': 7,
  '>': 7,
  '<=': 7,
  '>=': 7,
  '==': 7,
  '!=': 7,
  '+': 10,
  '-': 10,
  '*': 20,
  '/': 20,
  '%': 20
};

export class Parser {
  private tokenStream: TokenStream;
  constructor(input: string) {
    this.tokenStream = new TokenStream(input);
  }

  parseInput(): IProgramNode {
    const prog: Array<INode> = [];

    while (!this.tokenStream.eof()) {
      prog.push(this.parseExpression());
      if (!this.tokenStream.eof()) this.skipPunctuation(';');
    }

    return { type: 'prog', prog };
  }

  private parseExpression(): INode | ICallNode {
    return this.maybeCall(() => {
      return this.maybeBinary(this.parseAtom(), 0);
    });
  }

  private maybeCall(expressionParser: () => INode): INode | ICallNode {
    const expression = expressionParser.call(this);
    return this.isPunctuation('(')
      ? this.parseCall(<IVariableNode>expression)
      : expression;
  }

  private maybeBinary(left: INode, currentPrecidence: number): INode {
    const operatorToken = this.isOperator();

    if (operatorToken) {
      const nextPrecidence = PRECEDENCE[operatorToken.value];

      if (nextPrecidence > currentPrecidence) {
        this.tokenStream.next();

        return this.maybeBinary(
          <INode>{
            type: operatorToken.value === '=' ? 'assign' : 'binary',
            operator: operatorToken.value,
            left,
            right: this.maybeBinary(this.parseAtom(), nextPrecidence)
          },
          currentPrecidence
        );
      }
    }

    return left;
  }

  private parseAtom(): INode | ICallNode {
    return this.maybeCall(() => {
      if (this.isPunctuation('(')) {
        this.tokenStream.next();
        const expression = this.parseExpression();
        this.skipPunctuation(')');
        return expression;
      }

      if (this.isPunctuation('{')) return this.parseProgram();

      if (this.isKeyword('if')) return this.parseIf();

      if (this.isKeyword('true') || this.isKeyword('false'))
        return this.parseBool();

      if (this.isKeyword('lambda') || this.isKeyword('λ')) {
        this.tokenStream.next();
        return this.parseLambda();
      }

      const token = this.tokenStream.next();
      if (
        token &&
        (token.type === 'var' || token.type === 'num' || token.type === 'str')
      )
        return token;

      return this.unexpected();
    });
  }

  private isPunctuation(ch?: string): IPunctuationToken {
    const token = this.tokenStream.peek();

    return <IPunctuationToken>(token &&
      token.type === 'punc' &&
      (!ch || token.value === ch) &&
      token);
  }

  private skipPunctuation(ch: string): void {
    if (this.isPunctuation(ch)) this.tokenStream.next();
    else this.tokenStream.croak('Expecting punctuation: "' + ch + '"');
  }

  private isKeyword(keyword?: string): IKeywordToken {
    const token = this.tokenStream.peek();

    return <IKeywordToken>(token &&
      token.type === 'kw' &&
      (!keyword || token.value === keyword) &&
      token);
  }

  private skipKeyword(keyword: string): void {
    if (this.isKeyword(keyword)) this.tokenStream.next();
    else this.tokenStream.croak('Expecting keyword: "' + keyword + '"');
  }

  private isOperator(operator?: string): IOperatorToken {
    const token = this.tokenStream.peek();

    return <IOperatorToken>(token &&
      token.type === 'op' &&
      (!operator || token.value === operator) &&
      token);
  }

  private skipOperator(op: string): void {
    if (this.isOperator(op)) this.tokenStream.next();
    else this.tokenStream.croak('Expecting operator: "' + op + '"');
  }

  private unexpected(): never {
    return this.tokenStream.croak(
      'Unexpected token: ' + JSON.stringify(this.tokenStream.peek())
    );
  }

  private delimited<T>(
    start: string,
    stop: string,
    separator: string,
    parser: () => T
  ): Array<T> {
    const a: Array<T> = [];
    let first = true;

    this.skipPunctuation(start);

    while (!this.tokenStream.eof()) {
      if (this.isPunctuation(stop)) break;

      if (first) first = false;
      else this.skipPunctuation(separator);

      if (this.isPunctuation(stop)) break;

      a.push(parser.call(this));
    }

    this.skipPunctuation(stop);

    return a;
  }

  private parseCall(func: IVariableNode): ICallNode {
    return {
      type: 'call',
      func,
      args: this.delimited('(', ')', ',', this.parseExpression)
    };
  }

  private parseVariableName(): string {
    const name = <IVariableNode>this.tokenStream.next();

    if (name.type !== 'var') this.tokenStream.croak('Expecting variable name');

    return name.value;
  }

  private parseIf(): IIfNode {
    this.skipKeyword('if');

    const cond = this.parseExpression();

    if (!this.isPunctuation('{')) this.skipKeyword('then');

    const then = this.parseExpression();
    const ret: IIfNode = {
      type: 'if',
      cond,
      then
    };

    if (this.isKeyword('else')) {
      this.tokenStream.next();
      ret.else = this.parseExpression();
    }

    return ret;
  }

  private parseLambda(): ILambdaNode {
    return {
      type: 'lambda',
      vars: this.delimited('(', ')', ',', this.parseVariableName),
      body: this.parseExpression()
    };
  }

  private parseBool(): IBooleanNode {
    const token = this.tokenStream.next();

    return {
      type: 'bool',
      value: token ? token.value === true : false
    };
  }

  private parseProgram(): INode | IBooleanNode | IProgramNode {
    const prog = this.delimited('{', '}', ';', this.parseExpression);
    if (prog.length === 0) return FALSE;

    if (prog.length === 1) return prog[0];

    return { type: 'prog', prog };
  }
}
